import Nav from "./Nav";
import logo from "./logo.svg";
import "./App.css";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <div className="container">
      <Nav />
      <table className="table table-bordered table-striped">
        <thead className="thead-thick-border">
          <tr>
            <th className="text-center">Name</th>
            <th className="text-center">Conference</th>
          </tr>
        </thead>
        <tbody>
          {props.attendees.map((attendee) => {
            return (
              <tr key={attendee.href}>
                <td>{attendee.name}</td>
                <td>{attendee.conference}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default App;
