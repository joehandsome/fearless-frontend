window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/states";
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    const selectTag = document.getElementById("state");
    console.log(data);

    for (let state of data.states) {
      let option = document.createElement("option");
      option.value = state.abbreviation;
      option.innerHTML = state.name;
      selectTag.appendChild(option);
    }
  }
  const formTag = document.getElementById("create-location-form");
  formTag.addEventListener("submit", (event) => {
    event.preventDefault();
    //    console.log("need to submit the form data");
    const formData = new FormData(formTag);
    formData.append("state", document.getElementById("state").value);
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);
  });
});
