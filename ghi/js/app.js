//function createCard(name, description, pictureUrl) {
//  return `
//    <div class="card">
//      <img src="${pictureUrl}" class="card-img-top" alt="...">
//      <div class="card-body">
//        <h5 class="card-title">${name}</h5>
//        <p class="card-text">${description}</p>
//      </div>
//    </div>
//  `;
//}

function createCard(name, description, pictureUrl, date, venue) {
  return `
    <div class="col mb-4">
      <div class="card h-100 border-0 rounded-3 shadow">
        <img src="${pictureUrl}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${venue}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted border-0 bg-white">
          ${date}
      </div>
    </div>
  `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.log(`Error: ${response.status} - ${response.statusText}`);
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details);

          const date = details.conference.starts;
          const dateObject = new Date(date);
          const year = dateObject.getFullYear();
          const month = String(dateObject.getMonth() + 1).padStart(2, "0");
          const day = String(dateObject.getDate()).padStart(2, "0");
          const formattedDate = `${year}/${month}/${day}`;

          const title = details.conference.name;
          const venue = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;

          const html = createCard(
            title,
            description,
            pictureUrl,
            formattedDate,
            venue,
          );
          const container = document.querySelector(".row");

          container.insertAdjacentHTML("beforeend", html);
        }
      }
    }
  } catch (e) {
    console.log("Error: bruh you done messed up somewhere idk");
  }
});

//      const conference = data.conferences[0];
//      const nameTag = document.querySelector(".card-title");
//      nameTag.innerHTML = conference.name;
//
//      // console.log(data);
//        const details = await detailResponse.json();
//        console.log(details);
//
//        const pic = details.conference.location["picture_url"];
//        const picTag = document.querySelector(".card-img-top");
//
//        picTag.src = pic;
//
//        const desc = details.conference["description"];
//        const descTag = document.querySelector(".card-text");
//        descTag.innerHTML = desc;
//      } else {
//        console.log(`Error: ${detailResponse.status}`);
//      }
//    }
//  }
